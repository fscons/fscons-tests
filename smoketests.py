from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from assertpy import assert_that


def setup_module(module):
    """ setup any state tied to the execution of the given function.
    Invoked for every test function in the module.
    """
    global driver
    driver = webdriver.Firefox()

def teardown_module(module):
    """ teardown any state that was previously setup with a setup_function
    call.
    """
    global driver
    driver.close()

def test_frab():
    global driver
    driver.get("https://frab.fscons.org")
    assert_that(driver.title).contains("Conference Management")

def test_wiki():
    global driver
    driver.get("https:wiki.fscons.org")
    assert_that(driver.title).is_equal_to("FSCONS wiki")

def test_www():
    global driver
    driver.get("https://fscons.org")
    assert_that(driver.page_source).contains("Free Society Conference and Nordic Summit")

def test_blog():
    global driver
    driver.get("http://blog.fscons.org")
    assert_that(driver.page_source).contains("FSCONS Blog")

def test_freebeer():
    global driver
    driver.get("http://freebeer.fscons.org")
    assert_that(driver.page_source).contains("Download FREE BEER")

def test_subscribe_announces():
    global driver
    driver.get("https://fscons.org/")
    driver.find_element_by_name("email").send_keys("foobar@example.net")
    driver.find_element_by_id("subscribe-button").click()
    assert_that(driver.page_source).contains("You've made a subscription request to announces.")
